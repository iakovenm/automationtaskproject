package sell.com.zendesk;
import org.junit.jupiter.api.Test;
import static sell.com.zendesk.Data.*;
import static sell.com.zendesk.pages.LeadStatusChange.*;


public class LeadStatusRenameandCheckTest{

    @Test
    public void testLoginAddAndCheck() {
        openZendeskSellpage();
        login(emailDecoding(),passwordDecoding() );
        clickAddDroplist();
        clickLeadOption();
        inputLastname("Iakovenko");
        clickSaveAndViewLeadButton();
        assertStatusName("New");
        openUserSettings();
        openLeadSettings();
        openLeadStatusesSettings();
        editNewStatusName("BrandNew");
        navigateToLeads();
        openLeadDetails();
        refreshPage();
        assertStatusName("BrandNew");
    }
}










































































































































































































