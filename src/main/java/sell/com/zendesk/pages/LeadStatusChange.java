package sell.com.zendesk.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LeadStatusChange {

    public static SelenideElement addDroplist = $(".Riu--GlobalAdd--container span ._30z--Button--content");
    public static SelenideElement userEmail =  $("#user_email");
    public static SelenideElement userPassword =$("#user_password");
    public static SelenideElement saveAndViewLeadButton = $("._1fc--grid--align_center button[data-action=\"save-and-visit\"] ._30z--Button--content");
    public static SelenideElement lastnameInputField =$(by("placeholder","Last Name"));
    public static SelenideElement leadOptionInList=$("div._2Xz--Item--Item:nth-child(1)");
    public static SelenideElement statusName = $("span.lead-status");
    public static SelenideElement settingslist =$("#user-dd > a:nth-child(1)");
    public static SelenideElement settingsOptionInlist=  $("li.settings > a:nth-child(1)");
    public static SelenideElement customizeLeadsSettingsOption = $(".leads > a:nth-child(1)");
    public static SelenideElement leadStatusesSettingsTab =  $("#leads-settings-tabs > li:nth-child(3) > a:nth-child(1)");
    public static SelenideElement editStatusButton =$("#lead-status > div:nth-child(2)>span:first-child .edit");
    public static SelenideElement valueForStatusName =$("div.form div:nth-child(2) .controls .input-xlarge#name[data-current-value]");
    public static SelenideElement leadsTopMenuOption =$("#nav-working-leads") ;
    public static SelenideElement lastAddedleadsItem =$("div.NUZ--IndexItemRow--IndexItemRow:last-child > div:nth-child(2)");


    public static void openZendeskSellpage() {
        open("https://core.futuresimple.com/users/login");

    }

    public static void login(String email, String password) {
        userEmail.setValue(email).pressTab();
        userPassword.setValue(password).pressEnter();

    }

    public static void clickAddDroplist() {
        addDroplist.click();

    }
    public static void clickSaveAndViewLeadButton()  {

        saveAndViewLeadButton.click();

    }

    public static void clickLeadOption() {

        leadOptionInList.click();

    }


    public static void inputLastname(String lastname) {

        lastnameInputField.setValue(lastname);
    }

    public static void assertStatusName(String status) {
        statusName.shouldHave(text(status));

    }

    public static void openUserSettings(){
        settingslist.click();
        settingsOptionInlist.click();

    }

    public static void openLeadSettings(){
        customizeLeadsSettingsOption.click();
    }
    public static void openLeadStatusesSettings(){
        leadStatusesSettingsTab.click();
    }

    public static void editNewStatusName(String newName){
        editStatusButton.click();
        valueForStatusName.setValue(newName).pressEnter();


    }
    public static void navigateToLeads(){
        leadsTopMenuOption.click();

    }

    public static void openLeadDetails(){
        lastAddedleadsItem.click();

    }
    public static void refreshPage() {
       Selenide.refresh();
    }


}
